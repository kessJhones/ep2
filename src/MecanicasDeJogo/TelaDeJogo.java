package MecanicasDeJogo;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class TelaDeJogo extends JPanel implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Create the panel.
	 */
	static Button[] cobra;
	private Graphics delimitador;
	
	public TelaDeJogo() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(null);
		Star tipocobra = new Star();
		
		cobra = new Button[100];
		cobra[0] = new Button();
	    cobra[0].setBounds(150,150,tipocobra.getAltura(),tipocobra.getLargura());
	    cobra[0].setBackground(tipocobra.getCorCabeça());
		add(cobra[0]);

	/*	for(int i=1;i<5;i++) {
			cobra[i] = new Button();
			cobra[i].setBounds(150+10*i,150,tipocobra.getAltura(),tipocobra.getLargura());
			cobra[i].setBackground(tipocobra.getCorCorpo());
			add(cobra[i] );*/
		
		//}
		
	    cobra[0].addKeyListener(this);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int y[]= new int[100];
		int x[] = new int[100];
	    delimitador = getGraphics();
	    delimitador.drawRect(19,19,461,431);
		
		for(int i=0;i<6;i++){
			x[i]=cobra[i].getX();
			y[i]=cobra[i].getY();
			
			int posicao = e.getKeyCode();
			
			if(posicao==37) {
				if(i==0)
					cobra[i].setBounds(x[i]- 10,y[i],10,10);
				else
					cobra[i].setBounds(x[i-1],y[i-1],10,10);
			}
			else if(posicao==38) {
				if(i==0)
					cobra[i].setBounds(x[i],y[i]-10,10,10);
				else
					cobra[i].setBounds(x[i-1],y[i-1],10,10);
			}
			else if(posicao==39) {
				if(i==0)
					cobra[i].setBounds(x[i]+10,y[i],10,10);
				else
					cobra[i].setBounds(x[i-1],y[i-1],10,10);
			}
			else if(posicao==40) {
				if(i==0)
					cobra[i].setBounds(x[i],y[i]+10,10,10);
				else
					cobra[i].setBounds(x[i-1],y[i-1],10,10);
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
