package MecanicasDeJogo;

import java.awt.Color;

public abstract class Fruta {
	protected int pontuacao;
	protected int x,y;
	protected Color cor;
	
	public int getPontuacao() {
		return pontuacao;
	}
	public Color getCor() {
		return cor;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
}
