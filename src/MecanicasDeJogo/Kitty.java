package MecanicasDeJogo;

import java.awt.Color;

public class Kitty extends Cobra implements CobraJogavel{
	
	public Kitty() {
		altura =10;
		largura =10;
		corCabeça = Color.green;
		corCorpo = Color.black;
	}

	
	public boolean Colisao(int x, int y, int borax, int borday) {
		return false;
	}

	public int cresce() {
		return 1;
	}

}
