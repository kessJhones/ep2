package MecanicasDeJogo;

import java.awt.Color;

public abstract class Cobra {
	 protected int altura;
	 protected int largura;
	 protected Color corCabeça;
	 protected Color corCorpo;
	 
	public int getAltura() {
		return altura;
	}
	public int getLargura() {
		return largura;
	}
	public Color getCorCabeça() {
		return corCabeça;
	}
	public Color getCorCorpo() {
		return corCorpo;
	}
}
